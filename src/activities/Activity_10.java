package activities;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_10 {
	WebDriver driver = new FirefoxDriver();

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() 
     {
          //Open browser
    driver.get("https://alchemy.hguy.co/jobs/wp-admin");
    
     }
    @Test
	public void NewUser() {
     WebElement user=driver.findElement(By.id("user_login"));
   	 user.sendKeys("root");
   	 WebElement password=driver.findElement(By.id("user_pass"));
   	 password.sendKeys("pa$$w0rd");
   	 //Clicking Login button
	 driver.findElement(By.id("wp-submit")).click();
	 
	//Click on User
	 WebElement users=driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[11]/a"));
	 users.click();
	 
    //Add New user
	 WebElement addUsers=driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/a"));
	 addUsers.click();
	 
    
    driver.findElement(By.id("user_login")).sendKeys("nivbehl"); 		
    driver.findElement(By.id("email")).sendKeys("nivedita@gmail.com");   
    driver.findElement(By.id("first_name")).sendKeys("nivedita");  		
    driver.findElement(By.id("last_name")).sendKeys("behl"); 		
    driver.findElement(By.xpath("//*[@id=\"url\"]")).sendKeys("www.gmail.com");		
    
    //Click on Password Button
    WebElement Password=driver.findElement(By.cssSelector(".wp-generate-pw"));
    		Password.click();
       
    WebDriverWait wait = new WebDriverWait(driver, 20);
	wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("div#pass-strength-result"),"Strong"));
	
	//Enter new user password
	WebElement Password1 = driver.findElement(By.cssSelector("input#pass1-text"));
	Password1.sendKeys("nivbehl");

   
    driver.findElement(By.xpath("//*[@id=\"send_user_notification\"]")).click();    
    //Select Role
    Select Role = new Select(driver.findElement(By.xpath("//*[@id=\"role\"]")));
    Role.selectByIndex(2);
    
    //Add New user
    WebElement Button=driver.findElement(By.xpath("//*[@id=\"createusersub\"]"));
    Button.click();
    
    String msg = driver.findElement(By.cssSelector("#message")).getText();
    
    //Assert message
    Assert.assertTrue(msg.contains("New user created."),"New user created.");
     }
    
    @AfterMethod(alwaysRun = true)
    public void close() {
	 driver.quit();
    }
}

