package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_7{

	WebDriver driver = new FirefoxDriver();

	
	    @BeforeMethod
        public void beforeMethod() 
	     {
              //Open browser
        driver.get("https://alchemy.hguy.co/jobs/");
        
         }
	
    @Test
    public void Menu1() 
        {
    	WebElement Menu1= driver.findElement(By.cssSelector("#menu-item-26 > a:nth-child(1)"));
                        
        //Click the menu
        Menu1.click();
        WebElement email=driver.findElement(By.cssSelector("#create_account_email"));
		email.sendKeys("nivedita.behl@gmail.com");
        
     
        WebElement jobTitle= driver.findElement(By.cssSelector("#job_title"));
        jobTitle.sendKeys("Manager");
		WebElement jobLocation= driver.findElement(By.cssSelector("#job_location"));
		 jobLocation.sendKeys("Pune");
		 
		Select JobType=new Select(driver.findElement(By.id("job_type")));
		JobType.selectByIndex(0);
		
		
		    driver.switchTo().frame(0);
			WebElement jobDesc = driver.findElement(By.cssSelector("body#tinymce"));
			jobDesc.sendKeys("Manager for Testing");
			
			driver.switchTo().defaultContent();
			
			//Enter Application Email/URL
			WebElement appEmail = driver.findElement(By.xpath("//input[@id='application']"));
			appEmail.sendKeys("naukari@gmail.com");
			
			//Enter Company name
			WebElement compName = driver.findElement(By.cssSelector("#company_name"));
			compName.sendKeys("IBM");
			
			//Click Preview button
			WebElement buttonPreview = driver.findElement(By.cssSelector("input.button:nth-child(4)"));
			buttonPreview.click();
			
			//Click Preview button
			WebElement buttonSubmit = driver.findElement(By.xpath("//*[@id=\"job_preview_submit_button\"]"));
			buttonSubmit.click();
						
			//Verify success message
			WebElement successMsg = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div"));
			Assert.assertEquals("Job submitted successfully. Your listing will be visible once approved.", successMsg.getText());

        }
		
		
    
    @AfterMethod
    public void afterMethod() 
    {
        //Close the browser
       //driver.quit();
        
    }
}

