package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



public class Activity_9 {

	WebDriver driver = new FirefoxDriver();

	
	    @BeforeMethod(alwaysRun = true)
        public void beforeMethod() 
	     {
              //Open browser
        driver.get("https://alchemy.hguy.co/jobs/wp-admin");
        
         }
		  
     @Test
	 public void createNewListing() {
	     //Login
    	 WebElement user=driver.findElement(By.id("user_login"));
    	 user.sendKeys("root");
    	 WebElement password=driver.findElement(By.id("user_pass"));
    	 password.sendKeys("pa$$w0rd");
    	 //Click on login button
	     driver.findElement(By.id("wp-submit")).click();
	    
     //Open A Job Listing
     driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/ul/li[7]/a")).click();
     
     //Open Add New
     WebElement Addnew=driver.findElement(By.cssSelector(".page-title-action"));
     Addnew.click();
     
     //Tooltips
     driver.findElement(By.id("post-title-0")).click();
     driver.findElement(By.cssSelector(".nux-dot-tip__disable > svg:nth-child(1)")).click();
     
     //Add the data
     driver.findElement(By.id("post-title-0")).sendKeys("Tester");
     driver.findElement(By.cssSelector("#_job_location")).sendKeys("Pune");
     driver.findElement(By.id("_company_name")).sendKeys("IBM");
     driver.findElement(By.id("_company_website")).sendKeys("www.naukari.com");
     driver.findElement(By.id("_filled")).click();
     driver.findElement(By.id("_featured")).click();
     
     //Publish the results
     
     WebElement PublishButton = driver.findElement(By.cssSelector(".editor-post-publish-panel__toggle"));
     
     PublishButton.click();
     
     WebElement PublishFinal = driver.findElement(By.cssSelector(".editor-post-publish-button"));
	 PublishFinal.click();
    
     
        //Wait for Listing to be Published
   		WebDriverWait wait = new WebDriverWait(driver, 20);
   		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("div.editor-post-publish-panel__header-published"),"Published"));
   		
   		//Assert the Job being Published
   		Assert.assertEquals(driver.findElement(By.xpath("//div[contains(@class,'editor-post-publish-panel__header-published')]")).getText(),"Published");
   		
     } 
     
     
     
     @AfterMethod(alwaysRun = true)
     public void browserclose() {
	 driver.quit();
     }
     }

