package activities;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class Activity_1 {
	
	
	WebDriver driver = new FirefoxDriver();
	
	    @BeforeMethod
        public void beforeMethod() 
	     {
              //Open browser
        driver.get("https://alchemy.hguy.co/jobs/");
         }
	
    @Test
    public void Title() 
        {
      // Check the title of the page
      String title = driver.getTitle();
          
      //Print the title of the page
      System.out.println("Page title is: " + title);
          
          //Assertion for page title
      Assert.assertEquals("Alchemy Jobs � Job Board Application", title);
        }
    
    @AfterMethod
    public void afterMethod() 
    {
        //Close the browser
        driver.quit();
        
    }
    }
