package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_4 {

	WebDriver driver = new FirefoxDriver();
	
	    @BeforeMethod
        public void beforeMethod() 
	     {
              //Open browser
        driver.get("https://alchemy.hguy.co/jobs/");
         }
	
    @Test
    public void Header2() 
        {
    	WebElement Header2 = driver.findElement(By.tagName("h2"));
              
        //Print for page heading
        System.out.println("Second Heading on the page is: " + Header2.getText());
      //Assertion for page heading
        Assert.assertEquals(Header2.getText(), "Quia quis non");
        }
    
    @AfterMethod
    public void afterMethod() 
    {
        //Close the browser
        driver.quit();
        
    }
}

