package activities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_8{

	WebDriver driver = new FirefoxDriver();

	
	    @BeforeMethod
        public void beforeMethod() 
	     {
              //Open browser
        driver.get("https://alchemy.hguy.co/jobs/wp-admin");
        
         }
		  
	  @Test
	  public void Login() {
		  //Login
		  driver.get("https://alchemy.hguy.co/jobs/wp-login.php");
		  driver.findElement(By.id("user_login")).sendKeys("root");
		  driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd");
		  driver.findElement(By.id("wp-submit")).click(); 
		  
		  //Check the title of page
		  String pageTitle = driver.getTitle();
		  System.out.println("Page Title : " + pageTitle);
		  
		  //Check if you are logged in correctly
		  Assert.assertEquals(pageTitle, "Dashboard � Alchemy Jobs � WordPress");
		  
	  }
	  
	  @AfterMethod
	    public void afterMethod() 
	    {
	        //Close the browser
	       driver.quit();
	        
	    }
	}



