package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_6{

	WebDriver driver = new FirefoxDriver();

	
	    @BeforeMethod
        public void beforeMethod() 
	     {
              //Open browser
        driver.get("https://alchemy.hguy.co/jobs/");
        
         }
	
    @Test
    public void Menu1() 
        {
    	WebElement Menu1= driver.findElement(By.cssSelector("#menu-item-24 > a:nth-child(1)"));
              
        //Print for page heading
        System.out.println("Menu name on the header is: " + Menu1.getText());
      
        //Assertion for menu
        Assert.assertEquals(Menu1.getText(), "Jobs");
        
        //Click the menu
        Menu1.click();
       
       //Print title of new page
        System.out.println("Title is: " + driver.getTitle());
        
       //Assertion for new page
        Assert.assertEquals(driver.getTitle(), "Jobs � Alchemy Jobs");
        WebElement box1= driver.findElement(By.cssSelector("#search_keywords"));
       
		box1.sendKeys("Manager");
		 WebElement box2= driver.findElement(By.cssSelector("#search_location"));
		 box2.sendKeys("Pune");
		 
		//Find and click the search button
		 WebElement searchButton= driver.findElement(By.xpath("//input[@type='submit']"));
		 searchButton.click();

		WebDriverWait wait = new WebDriverWait(driver, 10);
			
		
		//Wait for results to load
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".showing_jobs")));
		
		//Find and print the number of results
		String results = driver.findElement(By.cssSelector(".showing_jobs")).getText();
		System.out.println("Number of results: " + results);
		
		//Click the first option always
		WebElement firstSuggestion = driver.findElement(By.cssSelector("li.job_listing:nth-child(1)"));
		//Wait for the button to be clicked
		wait.until(ExpectedConditions.elementToBeClickable(firstSuggestion));
        firstSuggestion.click();
        WebElement ApplyNow = driver.findElement(By.cssSelector(".application_button"));
      
        ApplyNow.click();
        WebElement eMail = driver.findElement(By.cssSelector(".job_application_email"));
        String resultEmail=eMail.getText();
        System.out.println("Email is: " + resultEmail);
		 }
    
    @AfterMethod
    public void afterMethod() 
    {
        //Close the browser
       driver.quit();
        
    }
}

