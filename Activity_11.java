package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Activity_11 {

	WebDriver driver = new FirefoxDriver();

	 @BeforeMethod
     public void beforeMethod() 
	     {
           //Open browser
     driver.get("https://alchemy.hguy.co/jobs/");
      }
	
 @Test
 public void Menu1() 
     {
 	WebElement Menu1= driver.findElement(By.cssSelector("#menu-item-24 > a:nth-child(1)"));
           
     //Print for page heading
     System.out.println("Menu name on the header is: " + Menu1.getText());
   
     //Assertion for menu
     Assert.assertEquals(Menu1.getText(), "Jobs");
     
     //Click the menu
     Menu1.click();
		
     WebElement box1= driver.findElement(By.cssSelector("#search_keywords"));
     		box1.sendKeys("Manager");
		 WebElement box2= driver.findElement(By.cssSelector("#search_location"));
		 box2.sendKeys("Pune");
		 
		//Unchecking Freelance Checkbox
		WebElement freelanceChk = driver.findElement(By.xpath("//input[@id='job_type_freelance']"));
		if (freelanceChk.isSelected())
		{
			freelanceChk.click();
		}
		
		//Unchecking Intership Checkbox
		WebElement internshipChk = driver.findElement(By.xpath("//input[@id='job_type_internship']"));
		if (internshipChk.isSelected())
		{
			internshipChk.click();
		}				
		
		///Unchecking Parttime Checkbox
		WebElement ParttimeChk = driver.findElement(By.xpath("//input[@id='job_type_part-time']"));
		if (ParttimeChk.isSelected())
		{
			ParttimeChk.click();
		}
		
		//Unchecking Temporary Checkbox
		WebElement TemporaryChk = driver.findElement(By.xpath("//input[@id='job_type_temporary']"));
		if (TemporaryChk.isSelected())
		{
			TemporaryChk.click();
		}
		//Find and click the search button
		 WebElement searchButton= driver.findElement(By.xpath("//input[@type='submit']"));
		 searchButton.click();
		   
		 WebDriverWait wait = new WebDriverWait(driver, 10);
			
			
			//Wait for results to load
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".showing_jobs")));
			
			//Find and print the number of results
			String results = driver.findElement(By.cssSelector(".showing_jobs")).getText();
			System.out.println("Number of results: " + results);
			
			//Click the first option always
			WebElement firstSuggestion = driver.findElement(By.cssSelector("li.job_listing:nth-child(1)"));
			//Wait for the button to be clicked
			wait.until(ExpectedConditions.elementToBeClickable(firstSuggestion));
	        firstSuggestion.click();
	        
	    //Print Title in console
		WebElement title =  driver.findElement(By.xpath("//title")); 
		System.out.println("Title :"+title.getAttribute("innerHTML"));

		
		//Click on Apply job
		WebElement ApplyNow = driver.findElement(By.cssSelector(".application_button"));
	      ApplyNow.click();
		
		
	
}

@AfterMethod
public void afterMethod() 
{
    //Close the browser
   driver.quit();
    
}
}



